﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Effects : MonoBehaviour
{
    [SerializeField] private GameObject effectPrefab;
    [SerializeField] private int effectAmount = 10;

    readonly List<GameObject> effects = new List<GameObject>();
    private Camera cam;
    
    private void Awake()
    {
        for (int i = 0; i < effectAmount; i++)
        {
            CreateEffect();
        }
    }

    private void Start()
    {
       cam = Camera.main;
    }

    GameObject CreateEffect()
    {
        var obj = Instantiate(effectPrefab, transform);
        obj.SetActive(false);
        effects.Add(obj);
        return obj;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var pos = cam.ScreenToWorldPoint(Input.mousePosition);
            PlayEffect(pos);
        }
    }

    void PlayEffect(Vector2 pos)
    {
        var obj = FindOrCreateEffect();
        obj.transform.position = pos;
        obj.SetActive(true);
        StartCoroutine(DisableAfterDelay(1, obj));
    }

    GameObject FindOrCreateEffect()
    {
        var obj = effects.FirstOrDefault(g => !g.activeSelf);
        return obj == null ? CreateEffect() : obj;
    }

    IEnumerator DisableAfterDelay(float delay, GameObject obj)
    {
        yield return new WaitForSeconds(delay);
        obj.SetActive(false);
        var ps = obj.GetComponent<ParticleSystem>();
    }
}
